---
typora-copy-images-to: ../vca_project_summary
typora-root-url: ../vca_project_summary
---

VCA project summary
===================

## Introduction

With the introduction of notified eID schemas, thanks to eIDAS regulation, Europe made a big step forward in the identity management interoperability field. Yet these systems rely on centralized standard models and protocols (SAML being the most spread).

The enrichment of identity attribute with other types of attestations and claims, as of today, remains an unfulfilled promise: Federated Identity Model, upon which the whole eID system is based, isn’t enough agile to let new player come in and provide certified attributes services.

Italy is one of the main exemplifications: tough systems like SPID and CIE are gradually taking over the eID scenario, Attribute authorities, which should have provided attributes and claims about identities are, as of today, completely absent.

With the advent of SSI we have an extraordinary opportunity to deploy new technology standards to enrich eID: verifiable credential. Yet, we need to perfectly fit the regulatory trust model while still being compliant to eIDAS schemes.

But still SSI is a very complicated subject, both from the theoretical and technical point of view, mainly because it requires a deep understanding of blockchain and cryptography concepts.

Further on, each SSI project tends to propose its own solution as a closed platform, with no particular attention to interoperability and blockchain-independent solution approach.

## The Verifiable Credential Authority

Inspired by the traditional Certificate Authority software model, Verifiable Credential Authority (aka VCA) is a set of brand new and existing integrated tools, combined together in order to provide a service to allow a 3rd Party to issue and verify a verifiable credential, in a cross-blockchain, cross-identity wallet styles, and fully respectful of both de-facto and official standards such as W3C and eIDAS.

VCA is an SSI core infrastructure component, designed to enable business actors to easily enter the SSI playground in the roles of trust anchors, accountable 3rd parties that, on a day-by-day basis, operate in the market as trust provider and a single source of truth regarding facts, claims, matter and any sort of information about physical and juridical persons.

VCA provides its users with capabilities to issue and verify claims, with the same simplicity through which the Certificate Authority allows an organization to issue certificate via a standard protocol, possibly abstracted with respect to the underling cryptographic devices.

Until the regulatory frameworks shall adopt SSI, VCA provides a bridge between this new standard and the eIDAS one by allowing the customer to sign the credential with eIDAS qualified Seal and to verify it against the EUTSL chain of trust.

Lastly, VCA implements all the core process with attention to Audit and security, as it’s done in the traditional Enterprise PKI Software Solutions.

## VCA High Level Architecture

![](/vca_hla.png)



**Core Engine**: it provides capability to issue and verify Verifiable Credential (compiling to W3C 1.0 data model), Blockcerts Certificate and OpenBadges. It relies on the DLT/Blockchain wrapper for the commitment or resolution of DID records and cred revocation. It relies on the ontology catalogue for the selection and extension ofstandard structured data (cfr. Schema.org / json-ld) and credential offer templates.

**Ontology Catalogue**: provide a solid standard data model for all the most used attribute data, in json-ld format. Provide also a set of credential offer and credential proof templates

**DLT/Blockchain Wrapper**: provides an abstract layer to interface, via specific implementation, any possible DLT/blockchain technology. Integrate universal resolver in order to support as many as possible did-method.

**eIDAS eSignature Wrapper**: provides an abstract layer to integrate, via specific implementation, eIDAS eSignature services provided by Qualified Trust Service Providers, starting from the Qualified eSeal Service by Infocert S.p.A.

**Integration API**: a set of API to allow seamless integration in customer Web/Mobile App or back end Services

**Web Front End**: provide web interface for Users and Admins. Users will be able to carry out simple task (like download or verify their VC or blockcerts certificates) Admins will be able to configure services and check logs.

**Identity provider**: a standard (open Source9 identity provider in order to allow integration with customers, particularly in the enterprise segment. The IdP supports the most user protocols (SAML, OAuth2.0 and OpenID Connect)

**Persistence Layer**: the data base for the service, storing all the produced objects and service configurations. It is the base repository for Accounting and Billing.

**Audit Journal**: provide secure registration of all the events in the systems, in order to allow external audit and integrity checks.

## 

